# Project Title

Fake-Twitter as a work interview

## Getting Started

In order to build and run Fake-Twitter you need maven. Please follow above steps:
- clone repository 
- navigate to dir Twitter
- run command mvn test in order to run test
- run command mvn spring-boot:run in order to run application. If any problem please check if port 8181 is free.
- please navigate to browser URL http://localhost:8181/swagger-ui.html in order to see OpenAPI documentation.

## Prerequisites
Build a simple social networking application, similar to Twitter, and expose it through a web API. The application should support the scenarios below.

Build a simple social networking application, similar to Twitter, and
expose it through a web API. The application should support the scenarios
below.

### Scenarios

#### Posting

A user should be able to post a 140 character message.

#### Wall

A user should be able to see a list of the messages they've posted, in reverse
chronological order.

#### Following

A user should be able to follow another user. Following doesn't have to be
reciprocal: Alice can follow Bob without Bob having to follow Alice.

#### Timeline

A user should be able to see a list of the messages posted by all the people
they follow, in reverse chronological order.

### Details

- use JAVA
- provide some documentation for the API, so that we know how to use it!
- don't care about registering users: a user is created as soon as they post
  their first message
- don't care about user authentication
- don't care about frontend, only backend
- don't care about storage: storing everything in memory is fine

### Submitting

Place your code on https://github.com.

