package com.jmatt.twitter.application;

import com.jmatt.twitter.api.dto.PostRequest;
import com.jmatt.twitter.domain.post.Post;
import com.jmatt.twitter.domain.post.PostRepository;
import com.jmatt.twitter.domain.user.User;
import com.jmatt.twitter.domain.user.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Streamable;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class UserProfileServiceTest {

    @Mock
    private PostRepository postRepository;
    @Mock
    private UserRepository userRepository;

    private UserProfileUseCase userProfileUseCase;

    @BeforeEach
    void setUp(){
        userProfileUseCase = new UserProfileService(userRepository, postRepository);
    }

    @Test
    void emptyWallIfAnyPostAddedByUser() {
        User user = mock(User.class);
        String username = "user123";
        Pageable pageRequest = PageRequest.of(1,20);
        given(userRepository.findByUsername(eq(username))).willReturn(Optional.of(user));
        given(postRepository.getPostsByUsername(any(), any())).willReturn(Page.empty());

        Page<Post> wall = userProfileUseCase.getWall(username, pageRequest);

        assertThat(wall.getTotalElements()).isEqualTo(0L);
    }

    @Test
    void wallWithAlreadyPostedMessagesInReverseChronologicalOrder() {
    }

    @Test
    void emptyTimelineIfUserHasNoFollowing() {
    }

    @Test
    void emptyTimelineIfUserHasFollowingWhoNotPublishPosts() {
    }

    @Test
    void timelineWithAlreadyPostedMessagesByFollowingInReverseChronologicalOrder() {
    }
}