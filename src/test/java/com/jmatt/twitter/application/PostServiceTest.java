package com.jmatt.twitter.application;

import com.jmatt.twitter.api.dto.PostRequest;
import com.jmatt.twitter.domain.post.MessageException;
import com.jmatt.twitter.domain.post.Post;
import com.jmatt.twitter.domain.post.PostRepository;
import com.jmatt.twitter.domain.user.User;
import com.jmatt.twitter.domain.user.UserRepository;
import com.jmatt.twitter.domain.user.UsernameException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class PostServiceTest {

    private static final String USERNAME_CANNOT_BE_EMPTY = "Username cannot be empty.";
    private static final String FORBIDDEN_WORDS_MESSAGE = "Usernames containing the words Twitter or Admin cannot be claimed. No account names can contain Twitter or Admin unless they are official Twitter accounts.";
    private static final String TOO_LONG_USERNAME_MESSAGE = "Your username cannot be longer than 15 characters.";
    private static final String FORBIDDEN_CHARACTERS_USERNAME_MESSAGE = "A username can only contain alphanumeric characters (letters A-Z, numbers 0-9) with the exception of underscores.";
    private static final String TOO_LONG_MESSAGE = "Your message cannot be longer than 140 characters.";

    @Mock
    private PostRepository postRepository;
    @Mock
    private UserRepository userRepository;

    private PostUseCase postUseCase;

    @BeforeEach
    void setUp(){
        postUseCase = new PostService(postRepository, userRepository);
    }

    @Test
    void cannotAddPostWhenEmptyUsername(){
        PostRequest postRequest = new PostRequest("", "No important message");

        assertThatThrownBy(()->postUseCase.addPost(postRequest))
                .isInstanceOf(UsernameException.class)
                .hasMessage(USERNAME_CANNOT_BE_EMPTY);
    }

    @Test
    void cannotAddPostWhenToLongUsername(){
        PostRequest postRequest = new PostRequest("1234567890_USER_12345", "No important message");

        assertThatThrownBy(()->postUseCase.addPost(postRequest))
                .isInstanceOf(UsernameException.class)
                .hasMessage(TOO_LONG_USERNAME_MESSAGE);
    }

    @Test
    void cannotAddPostWhenUsernameContainsSpecialCharacters(){
        PostRequest postRequest = new PostRequest("%USER%", "No important message");

        assertThatThrownBy(()->postUseCase.addPost(postRequest))
                .isInstanceOf(UsernameException.class)
                .hasMessage(FORBIDDEN_CHARACTERS_USERNAME_MESSAGE);
    }

    @Test
    void cannotAddPostWhenUsernameContainsForbiddenWords(){
        PostRequest postRequest = new PostRequest("Twitter", "No important message");

        assertThatThrownBy(()->postUseCase.addPost(postRequest))
                .isInstanceOf(UsernameException.class)
                .hasMessage(FORBIDDEN_WORDS_MESSAGE);
    }

    @Test
    void canAddPostWhenMessageIsToLong(){
        PostRequest postRequest = new PostRequest("correctUsername", "The code should always be simple. Complicated logic for achieving simple tasks is something you want to avoid as the logic one programmer implemented a requirement may not make perfect sense to another. So, always keep the code as simple as possible.");

        assertThatThrownBy(()->postUseCase.addPost(postRequest))
                .isInstanceOf(MessageException.class)
                .hasMessage(TOO_LONG_MESSAGE);
    }

    @Test
    void canAddPost(){
        PostRequest postRequest = new PostRequest("correctUsername", "Correct message");
        Post mockedPost = mock(Post.class);
        given(mockedPost.getUsername()).willReturn("correctUsername");
        given(mockedPost.getMessage()).willReturn("Correct message");
        given(postRepository.save(any(Post.class))).willReturn(mockedPost);

        Post post = postUseCase.addPost(postRequest);

        assertThat(post)
                .isNotNull()
                .hasFieldOrPropertyWithValue("username", "correctUsername")
                .hasFieldOrPropertyWithValue("message", "Correct message");
    }
}