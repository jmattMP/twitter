package com.jmatt.twitter.api.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmatt.twitter.api.dto.PostRequest;
import com.jmatt.twitter.application.PostUseCase;
import com.jmatt.twitter.domain.post.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class PostControllerTest {

    private static final String POST_URI = "/api/v1/posts";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PostUseCase postUseCase;

    @BeforeEach
    void setUp() {
    }

    @Test
    void addPost() throws Exception {
        PostRequest postRequest = new PostRequest("user123", "My first post on naive implementation of Twitter");

        mockMvc.perform(post(POST_URI)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(postRequest)))
                .andExpect(status().isCreated())
                .andExpect(content().string(org.hamcrest.Matchers.containsString(postRequest.getMessage())));
    }

    @Test
    void deletePost() throws Exception {
        PostRequest postRequest = new PostRequest("user123", "My first post on naive implementation of Twitter");

        Post post = postUseCase.addPost(postRequest);

        mockMvc.perform(delete(POST_URI + "/{postId}", post.getId())
                .contentType("application/json"))
                .andExpect(status().isNoContent());
    }
}