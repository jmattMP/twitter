package com.jmatt.twitter.api.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jmatt.twitter.api.dto.PostRequest;
import com.jmatt.twitter.application.PostUseCase;
import com.jmatt.twitter.application.UserProfileUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserProfileControllerTest {
    private static final String USER_PROFILE_URI = "/api/v1/user";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PostUseCase postUseCase;

    @Autowired
    private UserProfileUseCase userProfileUseCase;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getWall() throws Exception {
        String username = "user123";
        PostRequest postRequest = new PostRequest(username, "My first post on naive implementation of Twitter");
        PostRequest secondPostRequest = new PostRequest(username, "My second post on naive implementation of Twitter");
        PostRequest thirdPostRequest = new PostRequest(username, "My third post on naive implementation of Twitter");

        postUseCase.addPost(postRequest);
        postUseCase.addPost(secondPostRequest);
        postUseCase.addPost(thirdPostRequest);

        mockMvc.perform(get(USER_PROFILE_URI + "/{username}/wall", username)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(postRequest.getMessage())))
                .andExpect(content().string(containsString(secondPostRequest.getMessage())))
                .andExpect(content().string(containsString(thirdPostRequest.getMessage())));
    }

    @Test
    void getTimeline() throws Exception {
        String followerUsername = "user123";
        String followingUsername = "user356";
        String secondFollowingUsername = "user421";
        String thirdFollowingUsername = "user543";
        String notFollowingUsername = "user989";

        PostRequest followerUserRequest = new PostRequest(followerUsername, "First post of follower");
        PostRequest followingUserRequest = new PostRequest(followingUsername, "First post of following");
        PostRequest secondFollowingUserRequest = new PostRequest(secondFollowingUsername, "First post of second following");
        PostRequest thirdFollowingUserRequest = new PostRequest(thirdFollowingUsername, "First post of third following");
        PostRequest notFollowingUserRequest = new PostRequest(notFollowingUsername, "First post of not followed user");

        postUseCase.addPost(followerUserRequest);
        postUseCase.addPost(followingUserRequest);
        postUseCase.addPost(secondFollowingUserRequest);
        postUseCase.addPost(thirdFollowingUserRequest);
        postUseCase.addPost(notFollowingUserRequest);

        userProfileUseCase.follow(followerUsername, followingUsername);
        userProfileUseCase.follow(followerUsername, secondFollowingUsername);
        userProfileUseCase.follow(followerUsername, thirdFollowingUsername);

        mockMvc.perform(get(USER_PROFILE_URI + "/{username}/timeline", followerUsername)
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(followingUserRequest.getMessage())))
                .andExpect(content().string(containsString(secondFollowingUserRequest.getMessage())))
                .andExpect(content().string(containsString(thirdFollowingUserRequest.getMessage())));
                /*.andExpect(content().string(not(containsString(followerUserRequest.getMessage()))))
                .andExpect(content().string(not(containsString(notFollowingUserRequest.getMessage()))));*/

    }

    @Test
    void followUser() throws Exception {
        String followerUsername = "user123";
        String followingUsername = "user356";

        PostRequest followerUserRequest = new PostRequest(followerUsername, "First post of follower");
        PostRequest followingUserRequest = new PostRequest(followingUsername, "First post of following");

        postUseCase.addPost(followerUserRequest);
        postUseCase.addPost(followingUserRequest);

        mockMvc.perform(post(USER_PROFILE_URI + "/{followerUsername}/follow/{followingUsername}", followerUsername, followingUsername)
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    void unfollowUser() throws Exception {
        String followerUsername = "user123";
        String followingUsername = "user356";

        PostRequest followerUserRequest = new PostRequest(followerUsername, "First post of follower");
        PostRequest followingUserRequest = new PostRequest(followingUsername, "First post of following");

        postUseCase.addPost(followerUserRequest);
        postUseCase.addPost(followingUserRequest);

        mockMvc.perform(post(USER_PROFILE_URI + "/{followerUsername}/follow/{followingUsername}", followerUsername, followingUsername)
                .contentType("application/json"))
                .andExpect(status().isOk());

        mockMvc.perform(post(USER_PROFILE_URI + "/{followerUsername}/unfollow/{followingUsername}", followerUsername, followingUsername)
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

}