package com.jmatt.twitter;

import com.jmatt.twitter.api.dto.PostRequest;
import com.jmatt.twitter.application.PostUseCase;
import com.jmatt.twitter.application.UserProfileUseCase;
import com.jmatt.twitter.domain.post.Post;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class TwitterApplicationTests {

    @Autowired
    private PostUseCase postUseCase;

    @Autowired
    private UserProfileUseCase userProfileUseCase;

    @Test
    @Transactional
    void usersAddPostsThenOneFollowAndHasAccessToWallAndTimeline() {
        String followerUsername = "user123";
        String followingUsername = "user356";
        String secondFollowingUsername = "user421";
        String thirdFollowingUsername = "user543";
        String notFollowingUsername = "user989";

        PostRequest followerUserRequest = new PostRequest(followerUsername, "First post of follower");
        PostRequest followingUserRequest = new PostRequest(followingUsername, "First post of following");
        PostRequest secondFollowingUserRequest = new PostRequest(secondFollowingUsername, "First post of second following");
        PostRequest thirdFollowingUserRequest = new PostRequest(thirdFollowingUsername, "First post of third following");
        PostRequest notFollowingUserRequest = new PostRequest(notFollowingUsername, "First post of not followed user");

        Post followerPost = postUseCase.addPost(followerUserRequest);
        Post anotherFollowerPost = postUseCase.addPost(followerUserRequest);
        Post thirdFollowerPost = postUseCase.addPost(followerUserRequest);
        Post followingUserPost = postUseCase.addPost(followingUserRequest);
        Post secondFollowingPost = postUseCase.addPost(secondFollowingUserRequest);
        Post thirdFollowingPost = postUseCase.addPost(thirdFollowingUserRequest);
        Post notFollowingUserPost = postUseCase.addPost(notFollowingUserRequest);

        userProfileUseCase.follow(followerUsername, followingUsername);
        userProfileUseCase.follow(followerUsername, secondFollowingUsername);
        userProfileUseCase.follow(followerUsername, thirdFollowingUsername);

        Pageable pageable = PageRequest.of(0, 20, Sort.by("creationTime").descending());

        List<Long> wallPostIds = userProfileUseCase.getWall(followerUsername, pageable).getContent().stream().map(Post::getId).collect(Collectors.toList());
        List<Long> timelinePostIds = userProfileUseCase.getTimeline(followerUsername, pageable).stream().map(Post::getId).collect(Collectors.toList());

        assertThat(wallPostIds)
                .isNotNull()
                .containsExactly(thirdFollowerPost.getId(), anotherFollowerPost.getId(), followerPost.getId())
                .doesNotContain(followingUserPost.getId(), secondFollowingPost.getId(), thirdFollowingPost.getId(), notFollowingUserPost.getId());

        assertThat(timelinePostIds)
                .isNotNull()
                .containsExactly(thirdFollowingPost.getId(), secondFollowingPost.getId(), followingUserPost.getId())
                .doesNotContain(followerPost.getId(), notFollowingUserPost.getId());
    }

}
