package com.jmatt.twitter.application;

import com.jmatt.twitter.domain.post.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserProfileUseCase {

    Page<Post> getWall(String username, Pageable pageable);

    Page<Post> getTimeline(String username, Pageable pageable);

    void follow(String followerUsername, String followingUsername);

    void unfollow(String followerUsername, String followingUsername);
}
