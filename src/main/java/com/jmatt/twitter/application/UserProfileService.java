package com.jmatt.twitter.application;

import com.jmatt.twitter.domain.post.Post;
import com.jmatt.twitter.domain.post.PostRepository;
import com.jmatt.twitter.domain.user.User;
import com.jmatt.twitter.domain.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserProfileService implements UserProfileUseCase {
    private static final Logger logger = LoggerFactory.getLogger(UserProfileService.class);

    private final UserRepository userRepository;
    private final PostRepository postRepository;

    public UserProfileService(UserRepository userRepository, PostRepository postRepository) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }

    @Override
    public Page<Post> getWall(String username, Pageable pageable) {
        var optionalUser = userRepository.findByUsername(username);
        if (optionalUser.isEmpty()) {
            logger.info("User {} not found", username);
            return Page.empty();
        }
        return postRepository.getPostsByUsername(optionalUser.get().getUsername(), sortReverseChronological(pageable));
    }

    @Override
    public Page<Post> getTimeline(String username, Pageable pageable) {
        var optionalUser = userRepository.findByUsername(username);
        if (optionalUser.isEmpty()) {
            logger.info("User {} not found", username);
            return Page.empty();
        }
        List<String> followingUsernames = optionalUser.get().getFollowing().stream().map(User::getUsername).collect(Collectors.toList());
        if (followingUsernames.isEmpty()) {
            logger.info("User {} follow nobody", username);
            return Page.empty();
        }
        return postRepository.getPostsForUsers(followingUsernames, sortReverseChronological(pageable));
    }

    @Override
    @Transactional
    public void follow(String followerUsername, String followingUsername) {
        var optionalPerformerUser = userRepository.findByUsername(followerUsername);
        var optionalFollowingUser = userRepository.findByUsername(followingUsername);
        if (optionalPerformerUser.isEmpty()) {
            logger.info("Follower {} not found", followerUsername);
            return;
        }
        if (optionalFollowingUser.isEmpty()) {
            logger.info("Following {} not found", followerUsername);
            return;
        }
        var performer = optionalPerformerUser.get();
        var followingUser = optionalFollowingUser.get();
        performer.addFollowing(followingUser);
        userRepository.save(performer);
    }

    @Override
    @Transactional
    public void unfollow(String followerUsername, String followingUsername) {
        var optionalPerformerUser = userRepository.findByUsername(followerUsername);
        var optionalFollowingUser = userRepository.findByUsername(followingUsername);
        if (optionalPerformerUser.isEmpty()) {
            logger.info("Follower {} not found", followerUsername);
            return;
        }
        if (optionalFollowingUser.isEmpty()) {
            logger.info("Following {} not found", followerUsername);
            return;
        }
        optionalPerformerUser.get().getFollowing().removeIf(user -> user.getId() == optionalFollowingUser.get().getId());
    }

    private Pageable sortReverseChronological(Pageable pageable) {
        if(pageable.isUnpaged()){
            return Pageable.unpaged();
        }
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("creationTime").descending());
    }
}
