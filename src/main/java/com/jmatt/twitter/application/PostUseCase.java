package com.jmatt.twitter.application;

import com.jmatt.twitter.api.dto.PostRequest;
import com.jmatt.twitter.domain.post.Post;

public interface PostUseCase {
    Post addPost(PostRequest postRequest);

    void deletePost(long postId);
}
