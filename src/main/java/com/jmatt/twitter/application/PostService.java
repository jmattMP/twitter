package com.jmatt.twitter.application;

import com.jmatt.twitter.api.dto.PostRequest;
import com.jmatt.twitter.domain.post.MessageValidator;
import com.jmatt.twitter.domain.post.Post;
import com.jmatt.twitter.domain.post.PostRepository;
import com.jmatt.twitter.domain.user.User;
import com.jmatt.twitter.domain.user.UserRepository;
import com.jmatt.twitter.domain.user.UsernameValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
public class PostService implements PostUseCase {
    private static final Logger logger = LoggerFactory.getLogger(PostService.class);

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final UsernameValidator usernameValidator;
    private final MessageValidator messageValidator;

    public PostService(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
        usernameValidator = new UsernameValidator();
        messageValidator = new MessageValidator();
    }

    @Override
    @Transactional
    public Post addPost(PostRequest postRequest) {
        usernameValidator.validate(postRequest.getUsername());
        messageValidator.validate(postRequest.getMessage());

        addUser(postRequest.getUsername());

        var post = new Post();
        post.setUsername(postRequest.getUsername());
        post.setMessage(postRequest.getMessage());
        post.setCreationTime(LocalDateTime.now());
        return postRepository.save(post);
    }

    /*
     If Authentication will be added, remove add user -> creation of user should be separated form this context
    */
    private void addUser(String username){
        var optionalUser = userRepository.findByUsername(username);
        if(optionalUser.isEmpty()){
            logger.debug("User not exist. Creating new user with username {}", username);
            var user = new User();
            user.setUsername(username);
            userRepository.save(user);
        }
    }

    @Override
    @Transactional
    public void deletePost(long postId) {
        postRepository.deleteById(postId);
    }
}
