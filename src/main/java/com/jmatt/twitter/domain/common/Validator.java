package com.jmatt.twitter.domain.common;

public interface Validator<T> {
    void validate(T entity);
}