package com.jmatt.twitter.domain.post;

public class MessageException extends RuntimeException {
    public MessageException(String message) {
        super(message);
    }
}
