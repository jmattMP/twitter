package com.jmatt.twitter.domain.post;

import com.jmatt.twitter.domain.common.Validator;

public class MessageValidator implements Validator<String> {
    private static final String NO_EMPTY_MESSAGE = "No empty message allowed";
    private static final String TOO_LONG_MESSAGE = "Your message cannot be longer than 140 characters.";
    private static final int MAXIMUM_MESSAGE_LENGTH = 140;

    @Override
    public void validate(String entity) {
        if (null == entity || entity.isEmpty() || entity.isBlank()) {
            throw new MessageException(NO_EMPTY_MESSAGE);
        }

        if (entity.length() > MAXIMUM_MESSAGE_LENGTH) {
            throw new MessageException(TOO_LONG_MESSAGE);
        }
    }
}
