package com.jmatt.twitter.domain.post;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Long> {

    @Query("FROM Post p WHERE p.username = :username")
    Page<Post> getPostsByUsername(@Param("username") String username, Pageable pageable);

    @Query("FROM Post p WHERE p.username IN (:usernames)")
    Page<Post> getPostsForUsers(@Param("usernames") List<String> usernames, Pageable pageable);
}
