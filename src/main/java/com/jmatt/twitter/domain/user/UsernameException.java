package com.jmatt.twitter.domain.user;

public class UsernameException extends RuntimeException {
    public UsernameException(String message) {
        super(message);
    }
}
