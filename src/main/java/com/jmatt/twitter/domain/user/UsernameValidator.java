package com.jmatt.twitter.domain.user;

import com.jmatt.twitter.domain.common.Validator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class UsernameValidator implements Validator<String> {
    private final static Logger logger = LoggerFactory.getLogger(UsernameValidator.class);

    private static final Collection<String> FORBIDDEN_WORDS = List.of("TWITTER", "ADMIN");
    private static final String USERNAME_CANNOT_BE_EMPTY = "Username cannot be empty.";
    private static final String FORBIDDEN_WORDS_MESSAGE = "Usernames containing the words Twitter or Admin cannot be claimed. No account names can contain Twitter or Admin unless they are official Twitter accounts.";
    private static final String TOO_LONG_USERNAME_MESSAGE = "Your username cannot be longer than 15 characters.";
    private static final String FORBIDDEN_CHARACTERS_USERNAME_MESSAGE = "A username can only contain alphanumeric characters (letters A-Z, numbers 0-9) with the exception of underscores.";
    private static final int MAXIMUM_USERNAME_LENGTH = 15;

    @Override
    public void validate(String validateUsername) {
        logger.debug("Username {} is validating", validateUsername);
        if (null == validateUsername || validateUsername.isEmpty() || validateUsername.isBlank()) {
            logger.info(USERNAME_CANNOT_BE_EMPTY);
            throw new UsernameException(USERNAME_CANNOT_BE_EMPTY);
        }

        boolean isForbidden = checkIfForbiddenWord(validateUsername);
        boolean isTooLong = checkIfUsernameTooLong(validateUsername);
        boolean hasForbiddenCharacters = checkIfUsernameConsistForbiddenCharacters(validateUsername);

        if (isForbidden) {
            logger.info("Username {} cannot contain words like Twitter or Admin", validateUsername);
            throw new UsernameException(FORBIDDEN_WORDS_MESSAGE);
        }
        if (isTooLong) {
            logger.info("Username {} is to long", validateUsername);
            throw new UsernameException(TOO_LONG_USERNAME_MESSAGE);
        }
        if (hasForbiddenCharacters) {
            logger.info("Username {} contains forbidden characters ", validateUsername);
            throw new UsernameException(FORBIDDEN_CHARACTERS_USERNAME_MESSAGE);
        }
    }

    private boolean checkIfForbiddenWord(String validateUsername) {
        Optional<String> forbiddenUsername = FORBIDDEN_WORDS.stream().filter(s -> StringUtils.containsIgnoreCase(validateUsername, s)).findAny();
        if (forbiddenUsername.isPresent()) {
            return true;
        }
        return false;
    }

    private boolean checkIfUsernameTooLong(String username) {
        return username.length() > MAXIMUM_USERNAME_LENGTH;
    }

    private boolean checkIfUsernameConsistForbiddenCharacters(String username) {
        return !isAlphaNumericAndOrUnderscore(username);
    }

    private boolean isAlphaNumericAndOrUnderscore(String s) {
        String pattern = "[a-zA-Z0-9_]*";
        return s.matches(pattern);
    }
}
