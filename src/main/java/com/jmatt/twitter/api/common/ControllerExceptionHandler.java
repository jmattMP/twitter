package com.jmatt.twitter.api.common;


import com.jmatt.twitter.domain.post.MessageException;
import com.jmatt.twitter.domain.user.UsernameException;
import com.jmatt.twitter.domain.common.ErrorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(UsernameException.class)
    public ResponseEntity<ApiError> handleIncorrectUsername(UsernameException ex) {
        logger.info("Invalid username supplied in request. {}", ex.getMessage());
        ApiError apiError = new ApiError(ErrorType.USERNAME_ERROR, ex.getMessage(), LocalDateTime.now());
        return new ResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MessageException.class)
    public ResponseEntity<ApiError> handleIncorrectMessage(MessageException ex) {
        logger.info("Invalid message supplied in request. {}", ex.getMessage());
        ApiError apiError = new ApiError(ErrorType.MESSAGE_ERROR, ex.getMessage(), LocalDateTime.now());
        return new ResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }
}


