package com.jmatt.twitter.api.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jmatt.twitter.domain.common.ErrorType;

import java.time.LocalDateTime;

public class ApiError {

    private final ErrorType errorType;
    private final String errorMessage;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private final LocalDateTime timestamp;

    public ApiError(ErrorType errorType, String errorMessage, LocalDateTime timestamp) {
        this.errorType = errorType;
        this.errorMessage = errorMessage;
        this.timestamp = timestamp;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }
}
