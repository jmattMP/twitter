package com.jmatt.twitter.api.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class ApiDocumentationConfiguration {

    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfoV1())
                .groupName("v1")
                .select()
                .paths(PathSelectors.regex("\\/(api/v1.+)"))
                .build();
    }


    private ApiInfo apiInfoV1() {
        return new ApiInfoBuilder()
                .title("Open API Documentation")
                .description("Open API documentation of Fake-Twitter API")
                .contact(new Contact("Mateusz Pracik",
                        "https://www.linkedin.com/in/mateusz-pracik/",
                        "mateusz.pracik@gmail.com"))
                .version("1.0")
                .build();
    }
}
