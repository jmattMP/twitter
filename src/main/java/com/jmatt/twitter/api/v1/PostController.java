package com.jmatt.twitter.api.v1;

import com.jmatt.twitter.api.dto.PostMapper;
import com.jmatt.twitter.api.dto.PostRepresentation;
import com.jmatt.twitter.api.dto.PostRequest;
import com.jmatt.twitter.application.PostUseCase;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/posts")
@Api(tags = "post")
public class PostController {
    private static final Logger logger = LoggerFactory.getLogger(PostController.class);

    private final PostUseCase postUseCase;

    public PostController(PostUseCase postUseCase) {
        this.postUseCase = postUseCase;
    }

    @ApiOperation(value = "Adding new post")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PostRepresentation addPost(@RequestBody PostRequest postRequest) {
        logger.info("User {} is adding post", postRequest.getUsername());
        return PostMapper.toPostRepresentation(
                postUseCase.addPost(postRequest));
    }

    @ApiOperation(value = "delete post with given postId")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{postId}")
    public void deletePost(@PathVariable long postId) {
        logger.info("User is deleting post with postId {}", postId);
        postUseCase.deletePost(postId);
    }
}