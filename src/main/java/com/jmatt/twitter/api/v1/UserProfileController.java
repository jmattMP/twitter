package com.jmatt.twitter.api.v1;

import com.jmatt.twitter.application.UserProfileUseCase;
import com.jmatt.twitter.domain.post.Post;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/user")
@Api(tags = "user")
public class UserProfileController {

    private static final Logger logger = LoggerFactory.getLogger(UserProfileController.class);

    private final UserProfileUseCase userProfileUseCase;

    public UserProfileController(UserProfileUseCase userProfileUseCase) {
        this.userProfileUseCase = userProfileUseCase;
    }

    @ApiOperation(value = "Get wall of given user")
    @GetMapping("/{username}/wall")
    public Page<Post> getWall(@PathVariable String username, Pageable pageable) {
        logger.info("User {} is reading Wall", username);
        logger.debug("User {} is reading Wall on page {}  with size {}", username, pageable.getPageNumber(), pageable.getPageSize());
        return userProfileUseCase.getWall(username, pageable);
    }

    @ApiOperation(value = "Get timeline of given user")
    @GetMapping("/{username}/timeline")
    public Page<Post> getTimeline(@PathVariable String username, Pageable pageable) {
        logger.info("User {} is reading Timeline", username);
        logger.debug("User {} is reading Timeline on page {}  with size {}", username, pageable.getPageNumber(), pageable.getPageSize());
        return userProfileUseCase.getTimeline(username, pageable);
    }

    @ApiOperation(value = "Follow given user")
    @PostMapping("/{followerUsername}/follow/{followingUsername}")
    public void followUser(@PathVariable String followerUsername, @PathVariable String followingUsername) {
        logger.info("User {} is starting follow {}", followerUsername, followingUsername);
        userProfileUseCase.follow(followerUsername, followingUsername);
    }

    @ApiOperation(value = "Unfollow given user")
    @PostMapping("/{followerUsername}/unfollow/{followingUsername}")
    public void unfollowUser(@PathVariable String followerUsername, @PathVariable String followingUsername) {
        logger.info("User {} is stopping follow {}", followerUsername, followingUsername);
        userProfileUseCase.unfollow(followerUsername, followingUsername);
    }


}
