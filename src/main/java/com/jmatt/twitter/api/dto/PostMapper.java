package com.jmatt.twitter.api.dto;

import com.jmatt.twitter.domain.post.Post;

import java.util.Collection;
import java.util.stream.Collectors;

public class PostMapper {
    public static PostRepresentation toPostRepresentation(Post post) {
        return new PostRepresentation(post.getId(), post.getMessage(), post.getCreationTime());
    }

    public static Post toPost(PostRepresentation postRepresentation) {
        Post post = new Post();
        post.setId(postRepresentation.getPostId());
        post.setMessage(postRepresentation.getMessage());
        post.setCreationTime(postRepresentation.getCreationTime());
        return post;
    }

    public static Collection<PostRepresentation> toPostRepresentations(Collection<Post> posts) {
        return posts.stream().map(PostMapper::toPostRepresentation).collect(Collectors.toList());
    }
}
