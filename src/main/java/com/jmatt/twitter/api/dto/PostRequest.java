package com.jmatt.twitter.api.dto;

public class PostRequest {
    private final String username;
    private final String message;

    public PostRequest(String username, String message) {
        this.username = username;
        this.message = message;
    }

    public String getUsername() {
        return username;
    }

    public String getMessage() {
        return message;
    }
}
