package com.jmatt.twitter.api.dto;

import java.time.LocalDateTime;

public class PostRepresentation {
    private final long postId;
    private final String message;
    private final LocalDateTime creationTime;

    public PostRepresentation(long postId, String message, LocalDateTime creationTime) {
        this.postId = postId;
        this.message = message;
        this.creationTime = creationTime;
    }

    public long getPostId() {
        return postId;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }
}
